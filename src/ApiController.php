<?php

namespace GranitSDK;

use GranitSDK\ApiController\Exception\BadRequest;
use GranitSDK\ApiController\Exception;
use Phalcon\Events\Event;
use Phalcon\Mvc\Controller;

class ApiController extends Controller
{
	protected $errors = [];
	protected $useRawResponse = false;

	public function initialize()
	{
		$this->dispatcher->getEventsManager()->attach('dispatch:beforeException', $this);
		$this->dispatcher->getEventsManager()->attach('dispatch:afterExecuteRoute', $this);
	}

	protected function addBadRequest($error)
	{
		$this->errors[] = $error;
	}

	public function validateRequest()
	{
		if (!empty($this->errors)) {
			throw new BadRequest($this->errors);
		}
	}

	public function beforeException(Event $event, $dispatcher, \Exception $exception)
	{
		$statusCode = 500;
		$error = [
			'message' => $exception->getMessage(),
			'code' => $exception->getCode(),
		];

		if ($exception instanceof Exception) {
			$error = $exception->toArray();
			$statusCode = $exception->getCode() ?? 500;
		}

		if ($exception instanceof Exception\ServerError) {
			Logger::get()->error($error['message'], $error);
		}
		else {
			Logger::get()->warning($error['message'], $error);
		}

		$this->response->setJsonContent([
			'code' => $statusCode,
			'error' => $error,
			'body' => ''
		]);
		$this->dispatcher->setReturnedValue($this->response);
		return false;
	}

	public function beforeExecuteRoute()
	{

	}

	public function onConstruct()
	{
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			$this->response->setHeader('Access-Control-Allow-Origin', '*');
			$this->response->setHeader('Access-Control-Allow-Credentials', 'true');
			$this->response->setHeader('Access-Control-Max-Age', '86400');
		}

		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
				$this->response->setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, DELETE, PATCH');
			}

			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
				$this->response->setHeader('Access-Control-Allow-Headers', '*');
			}

			$this->response->sendHeaders();

			exit(0);
		}
	}

	public function afterExecuteRoute()
	{
		if ($this->useRawResponse) {
			return true;
		}

		if (!$this->response->getStatusCode()) {
			$this->response->setStatusCode(200);
		}

		$this->response->setJsonContent([
			'code' => $this->response->getStatusCode(),
			'error' => [],
			'body' => $this->dispatcher->getReturnedValue(),
		]);

		$this->dispatcher->setReturnedValue($this->response);
		return false;
	}
}
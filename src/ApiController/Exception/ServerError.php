<?php

namespace GranitSDK\ApiController\Exception;

use GranitSDK\ApiController\Exception;

class ServerError extends Exception
{
	public function __construct($message, array $context = [])
	{
		$context['exceptionMessage'] = $message;

		parent::__construct('Internal Server Error', 500, $context);
	}
}
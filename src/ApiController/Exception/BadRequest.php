<?php

namespace GranitSDK\ApiController\Exception;

use GranitSDK\ApiController\Exception;

class BadRequest extends Exception
{
	public function __construct(array $message = [])
	{
		parent::__construct('Bad Request', 400, $message);
	}
}
<?php

namespace GranitSDK\ApiController\Exception;

use GranitSDK\ApiController\Exception;

class Unauthorized extends Exception
{
	protected $messageList = [];

	public function __construct()
	{
		parent::__construct('Unauthorized', 401);
	}
}
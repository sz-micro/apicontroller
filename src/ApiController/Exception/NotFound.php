<?php

namespace GranitSDK\ApiController\Exception;

use GranitSDK\ApiController\Exception;

class NotFound extends Exception
{
	public function __construct($entity, $paramVaue)
	{
		parent::__construct('Not Found', 404, [
			"Not found [$entity] Entity by [$paramVaue] parameter"
		]);
	}
}
<?php

namespace GranitSDK\ApiController;


use Throwable;

class Exception extends \Exception
{
	protected $errorMessages = [];

	public function __construct($message, $code, $errorMessages = [])
	{
		$this->errorMessages = $errorMessages;
		parent::__construct($message, $code);
	}

	public function getErrorMessages(): array
	{
		return $this->errorMessages;
	}

	public function toArray()
	{
		return [
			'type' => $this->message,
			'code' => $this->code,
			'errorMessages' => $this->errorMessages,
		];
	}
}